import pickle
from pprint import pprint
import numpy as np
from io import StringIO

def get_raw_data(filename):
    f = open(filename, "r")
    header = f.readline().strip().split(";")
    pprint(header)
    data = [{key: value
             for key, value in zip(header, row.strip().split(";"))}
            for row in f.readlines() if not row.startswith("#")]
    return data

def get_typed_data(filename, key_funcs={}):
    np_load_str = lambda x,s: np.loadtxt(StringIO(x.strip()),
                                         delimiter=',').reshape(s)
    format_funcs = {
        'frame'      : lambda f: f,
        'time'       : lambda t: t,
        'name'       : lambda n: n,
        'box'        : lambda b: np_load_str(b, (-1)),
        'centre'     : lambda c: np_load_str(c, (-1)),
        'vector3D'   : lambda v: np_load_str(v, (2, 3)),
        'angle3D'    : lambda a: np_load_str(a, (-1)),
        'landmarks'  : lambda l: np_load_str(l, (68, 3)),
        'is_speaking': lambda i: None,
        'emotions'   : lambda e: None,
    }
    for k,v in key_funcs.items():
        format_funcs[k] = v
    print("=== Loading ===")
    raw = get_raw_data(filename)
    print("=== Formatting ===")
    data= list()
    for row in raw:
        new_row = {}
        for key, func in format_funcs.items():
            if key in row:
                new_row[key] = func(row[key])
        data.append(new_row)
    return data

def group_by_key(data, key):
    grouped_data = {d[key]: list() for d in data}
    for d in data:
        grouped_data[d[key]].append(d)
    return grouped_data

def filter_keys(data, keys=()):
    return [{key: d[key] for key in keys } for d in data]

def format(start=0,
           window=12,
           spread=1,
           stride=12,
           speaking_file=None,
           data_file=None,
           keys=("landmarks"),
           key_funcs={}
          ):
    if data_file is None:
        exit(1)
    frames = filter_keys(get_typed_data(data_file,
        key_funcs=key_funcs),
                         keys=("name","frame")+keys)
    grouped = group_by_key(frames, "name")
    if speaking_file is not None:
        speaking_data = list()
        with open(speaking_file, "r") as sf:
            for l in sf.readlines():
                l = l.strip().split(';')
                speaking_data.append({"frame"    : int(l[0]),
                                      "name"     : l[1],
                                      "speaking" : l[2],
                                      "type"     : l[3],
                                     })
        starts = {name: 0 for name in group_by_key(speaking_data, "name")}
        stype  = {name: 0 for name in group_by_key(speaking_data, "name")}
        max_frame = int(max(frames,
                            key=lambda f: int(f["frame"]))["frame"])
        speaking_result = {name:np.zeros(max_frame,
                                         dtype=int)
                           for name in group_by_key(speaking_data, "name")}
        speaking_result_type = {name:np.zeros(max_frame,
                                              dtype=int)
                                for name in group_by_key(speaking_data, "name")}
        val = 1
        for s in speaking_data:
            if s["speaking"] == "S-start":
                starts[s["name"]] = s["frame"]
                stype[s["name"]] = int(s["type"] == "Inter")*2 -1
            else:
                speaking_result[s["name"]][starts[s["name"]]-1:s["frame"]] = 1
                speaking_result_type[s["name"]][starts[s["name"]]-1:s["frame"]]=stype[s["name"]]

    for name in grouped:
        for i, face in enumerate(grouped[name]):
            frame = int(face["frame"])-1
            if speaking_file is not None:
                grouped[name][i]["speaking"] =      speaking_result[name][frame]
                grouped[name][i]["type"]     = speaking_result_type[name][frame]

    result = {}
    for name, session in grouped.items():
        maximum = len(session)-1
        result[name] = list()
        for i in range(start, 1+maximum-(window-1)*spread, stride):
            batch = list()
            for frame in range(i, i+window*spread, spread):
                batch.append(session[frame])
            result[name].append(batch)
        print(f"it(start={start}",
              f"maximum={maximum}",
              f"stride={stride}",
              f"window={window}",
              f"spread={spread})",
              sep=", ")
    ret = {k: {name: np.array([[f[k]
                                for f in batch]
                               for batch in session],
                              dtype=np.float64)
               for name, session in result.items()}
           for k in keys}
    if speaking_file is not None:
        ret["speaking"] = {name: np.array([[np.round(np.sum([f["speaking"]
                                                           for f in
                                                            batch])/(len(batch)-1))]
                                          for batch in session],
                                         dtype=np.int32)
                          for name, session in result.items()}
        tmp = {name:
               np.array([[np.round(np.sum([f["type"]
                                           for f in batch])/(len(batch)-1))]
                         for batch in session],
                        dtype=np.int32)
              for name, session in result.items()}
        ret["type"] = {name : np.where(session==-1, 2, session)
                           for name, session in tmp.items()}
    return ret

def load ():
#    res = format(stride=12, window=12,
#                 data_file="./00042/data-00042.csv",
#                 speaking_file="./00042/Speaking_Dyades_00042.csv",
#                 keys=("landmarks","vector3D"),
#                 key_funcs={"landmarks":
#                     lambda x: np.loadtxt(StringIO(x.strip()),
#                                          delimiter=','
#                                         ).reshape((68,3))[48:,:2].reshape((-1))}
#                )
#    with open('./00042/vector3D.pickle', 'wb') as handle:
#        pickle.dump(res["vector3D"], handle, protocol=pickle.HIGHEST_PROTOCOL)
#    with open('./00042/landmarks.pickle', 'wb') as handle:
#        pickle.dump(res["landmarks"], handle, protocol=pickle.HIGHEST_PROTOCOL)
#    with open('./00042/speaking_labels.pickle', 'wb') as handle:
#        pickle.dump(res["speaking"], handle, protocol=pickle.HIGHEST_PROTOCOL)
#    with open('./00042/dyade_labels.pickle', 'wb') as handle:
#        pickle.dump(res["type"], handle, protocol=pickle.HIGHEST_PROTOCOL)
    res = format(stride=12, window=12,
                 data_file="./3v2FR/data-3v2FR.csv",
                 speaking_file="./3v2FR/Speaking_Dyades_3v2FR.csv",
                 keys=("landmarks", "centre"),
                 key_funcs={"landmarks":
                     lambda x: np.loadtxt(StringIO(x.strip()),
                                          delimiter=','
                                         ).reshape((68,3))[48:,:2].reshape((-1))}
                )
    with open('./3v2FR/centres.pickle', 'wb') as handle:
        pickle.dump(res["centre"], handle, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ =="__main__":
    load()
# res["landmarks"]: dico reliant chaque nom à ses tranches de 12 frames de landmarks
# {'1': array([[[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]
#               ...
#              [[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]]),
#  '2': array([[[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]
#               ...
#              [[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]]),
#  '3': array([[[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]
#               ...
#              [[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]]),
#  '4': array([[[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]
#               ...
#              [[x1, y1, x2, y2 ...], ... (10 autres), [x1, y1, x2, y2 ...]]])}
# de meme pour les autres keys
# le format est specifié dans get_typed_data

# labels: dico reliant chaque nom à ses tranches de 12 frames de labels:
# {'1': array([[lbl], [lbl], ...])
#  '2': array([[lbl], [lbl], ...])
#  '3': array([[lbl], [lbl], ...])
#  '4': array([[lbl], [lbl], ...])}

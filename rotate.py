from numpy import arctan2, cos, sin
import numpy as np
import csv

def get_angle(orig, target, cmp_axis):
    Vn = np.cross(target, orig)
    norm = np.linalg.norm(Vn)
    if norm == 0:
        return 0
    Vn /= norm
    angle = arctan2(np.dot(np.cross(target, orig), Vn),
                    np.dot(orig, target))
    if orig[cmp_axis] < target[cmp_axis]:
        return angle
    else:
        return -angle

def rotate(obj, rx, ry, rz):
    rotx = np.array([[ 1      , 0      , 0      ],
                     [ 0      , cos(rx),-sin(rx)],
                     [ 0      , sin(rx), cos(rx)]])
    roty = np.array([[ cos(ry), 0      , sin(ry)],
                     [ 0      , 1      , 0      ],
                     [-sin(ry), 0      , cos(ry)]])
    rotz = np.array([[ cos(rz),-sin(rz), 0      ],
                     [ sin(rz), cos(rz), 0      ],
                     [ 0      , 0      , 1      ]])
    rot = np.matmul(np.matmul(rotz,roty),rotx)
    return np.dot(obj, rot.T)

def orient_normalize_3d_landmarks(lms):
    center = np.mean(lms, axis=0)
    lms -= center
    lms /= np.amax(np.linalg.norm(lms, axis=1))
    origx  = lms[30]*[0,1,1]
    origx /= np.linalg.norm(origx)
    targetx = np.array([0, 0, 1], dtype=np.float)
    targety = np.array([0, 0, 1], dtype=np.float)
    targetz = np.array([0, 1, 0], dtype=np.float)
    rx = -get_angle(orig=origx, target=targetx, cmp_axis=1)
    rotx = np.array([[ 1      , 0      , 0      ],
                     [ 0      , cos(rx),-sin(rx)],
                     [ 0      , sin(rx), cos(rx)]])
    origy, origz = np.dot(np.vstack((lms[30],lms[8])), rotx.T)
    origy  = origy*[1,0,1]
    origy /= np.linalg.norm(origy)
    ry = get_angle(orig=origy, target=targety, cmp_axis=0)
    roty = np.array([[ cos(ry), 0      , sin(ry)],
                     [ 0      , 1      , 0      ],
                     [-sin(ry), 0      , cos(ry)]])
    origz  = np.dot(origz,roty.T)
    origz  = origz*[1,1,0]
    origz /= np.linalg.norm(origz)
    rz = -get_angle(orig=origz, target=targetz, cmp_axis=0)
    rotz = np.array([[ cos(rz),-sin(rz), 0      ],
                     [ sin(rz), cos(rz), 0      ],
                     [ 0      , 0      , 1      ]])
    rot = np.matmul(np.matmul(rotz,roty),rotx)
    lms = np.dot(lms,rot.T)
    lms -= np.amin(lms, axis=0)
    lms /= np.amax(lms, axis=0)
    return (rx, ry, rz), lms

if __name__ == "__main__":
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax1 = fig.add_subplot(121, projection='3d')
    ax2 = fig.add_subplot(122)
    csvr = csv.reader(open("../dataset/train-3D-landmarks.csv"),delimiter=":")
    header = next(csvr)
    for i, l in enumerate(csvr):
            try:
                lms = np.array([float(lm) for lm in l[1].split(",")]).reshape((-1,3))
            except:
                continue
            lms = rotate(lms, *np.random.rand(3)*np.pi*2)
            ax1.clear()
            ax1.scatter(*lms.T, c='r', marker='o')
            _, lms = orient_normalize_3d_landmarks(lms)
            ax2.clear()
            ax2.scatter(*1-lms.T[:2], c='r', marker='o')
            plt.show(block=False)
            plt.pause(.01)
    plt.show()

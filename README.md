# Eureka Effect  
Emotion/Posture/Human Interaction detection from a video stream, to detect when someone understands a specified subject.  

# Règles  
- une seule fonctionnalité par branche  
- si vous travaillez à plusieurs sur une seule branche, libre à vous de combiner vos codes comme vous le souhaitez  
- @all, le développement ne se fait que dans les branches, et surtout pas master  
- @Soulthym s'occupe de merge les branches dans master  
- les messages de commit doivent décrire clairement les changements effectués depuis le dernier commit (croyez moi ça aide à debug x))  
- si vous avez besoin d'une fonctionnalité présente dans une autre branche, libre à vous de les fusionner, vous serez juste plus nombreux sur la même branche  
- décrivez dans un fichier les dépendances de vos fonctionnalités, ca fera gagner du temps à tout le monde ;) 

# Tips!  
plus vous commit souvent et clairement, plus vous avez de chance de trouver ce qui fait bugger votre code, et surtout de possibilités de rollback :)  
#! /bin/bash
optirun=$(which optirun | sed 's/\/usr\/bin\///g')
echo $optirun
video=$2
[ -z "$video" ] && video="videos/00042.MTS"
case $1 in
    full)
        $optirun python -u main.py --device cuda --input-file $video -n 2 1 0 3 0 4 0 #-m 100
        $optirun python -u emotion_classifier.py $video
        mv out/data-00042.csv{.out,}
        ;;
    cpu)
                 python -u main.py --device cpu  --input-file $video -n 2 1 0 3 0 4 0 --cli
                 python -u emotion_classifier.py $video
        mv out/data-00042.csv{.out,}
        ;;
    gpu)
        $optirun python -u main.py --device cuda --input-file $video -n 2 1 0 3 0 4 0 --cli
        $optirun python -u emotion_classifier.py $video
        mv out/data-00042.csv{.out,}
        ;;
    *)
                 python -u main.py --device cpu --input-file $video -n 2 1 0 3 0 4 0 -m 50
                 python -u emotion_classifier.py $video
        mv out/data-00042.csv{.out,}
        ;;
esac

from copy import copy
from decimal import Decimal
import time
import cv2
from face_detector import FaceIdentifier
from argparser import Parser
from io import StringIO
import numpy as np
from pprint import pprint, pformat
from progressbar import progressbar
from itertools import groupby
import pickle
from multiprocessing.dummy import Pool as ThreadPool

def load_tmp(tmpfile=".tmp/tmp", max_frame=None):
    print(f"Loading {tmpfile}")
    faces = []
    with open(tmpfile,'r') as tmpf:
        header = tmpf.readline().strip().split(";")
        print(f"loading tmp file : {pformat(header)}")
        for l in progressbar(tmpf.readlines()):
            face={}
            l = l.strip().split(";")
            box = np.loadtxt(StringIO(l[1]), delimiter=',')
            center = np.array([box[0] + box[2]/2, box[1] + box[3]/2])
            face = {"frame":int(l[0]),
                    "cen":center,
                    "box":box,
                    "emb":np.loadtxt(StringIO(l[2]), delimiter=','),
                    "lms":np.loadtxt(StringIO(l[3]), delimiter=',').reshape((-1,3))
            }
            if max_frame != None:
                if face["frame"] > max_frame:
                    print(f"\nStopping prematurely at frame {max_frame}")
                    break
            faces.append(face)
    return faces

def group_faces_by_frames(faces):
    print("Grouping by frame")
    frames={}
    for face in progressbar(faces):
        frame_nb = face.pop("frame")
        if frame_nb in frames.keys():
            frames[frame_nb].append(face)
        else:
            frames[frame_nb] =     [face]
    return frames

def spatio_temporal_tracking(frames):
    print("Tracking spatially and temporaly")
    time_tolerance = 10
    tracking = []
    tracked = []
    for face in frames[1]:
        tracking.append({1:face})

    for fnb, faces in progressbar(sorted(frames.items())[1:]):
        for face in faces:
            distance_tolerance = max(face["box"][2:4])/4
            for tface in tracking:
                # go through all the tracked faces, keep the first face found
                # within 20pixels of the last tracked face and append it to the
                # session
                dist = np.linalg.norm(tface[max(sorted(tface))]["cen"]
                                      -face["cen"])
                if dist < distance_tolerance:
                    tface[fnb] = face
                    break
            else:
                # if the face isnt close to any of the already tracked faces, add
                # it to the list of tracked faces as a new session
                tracking.append({fnb:face})
        for tface in copy(tracking):
            # all the tracked faces that weren't updated are removed from the list
            # of running sessions and added to the list of finished sessions
            if max(sorted(tface)) + time_tolerance < fnb:
                tracked.append(tface)
                tracking.remove(tface)
    for tface in copy(tracking):
        # all the tracked faces are removed from the list of running sessions and
        # added to the list of finished sessions
        tracked.append(tface)
        #tracking.remove(tface)
    return tracked

def create_face_identifier_from_known_sessions(names, frames, tracked):
    FI = FaceIdentifier()
    known_sessions = list()
    names = {n:center for n,center in zip(names, frames[1]) if n!= 0 }
    for name,face in names.items():
        known_sessions.append({
            "name":name,
            "session":next(tface for tface in tracked if 1 in tface
                   and (tface[1]["cen"] == face["cen"]).all())
        })
    for ksession in known_sessions:
        if ksession["name"] != '0':
            print(f"creating profile for {ksession['name']}")
            FI.newPerson(ksession["name"])
            for _,face in progressbar(ksession["session"].items()):
                FI.train(name=ksession["name"], ID=face["emb"])
        else:
            print(f"not keeping {ksession['name']}")
    FI.buildDB()
    return FI

def identify_in_tracked_sessions(tracked, FI):
    # identifying people
    print("Identification")
    for i, session in progressbar(enumerate(tracked)):
        for fnb, face in session.items():
            face["dist"], face["name"] = FI.identify(face["emb"])
    return tracked

def calculate_confidence_score(tracked, embedding_tolerance=0.67):
    # averaging the names
    named_frames = {}
    for session in tracked:
        counter = {"unknown": 0}
        for face in session.values():
            if 1-face["dist"] > embedding_tolerance:
                if face["name"] not in counter:
                    counter[face["name"]] = 1
                else:
                    counter[face["name"]] = counter[face["name"]] + 1
            else:
                counter["unknown"] = counter["unknown"] + 0.2
        names = list(sorted(counter.items(), key=lambda kv: -kv[1]))#[0][0]
        name = names[0][0]
        for fnb, face in session.items():
            face["name"] = name
            face["conf"] = counter[name]/sum(counter.values()) * \
                           (1-(float(Decimal(1)/np.exp(Decimal(sum(counter.values())/10)))))
            if fnb in named_frames:
                named_frames[fnb].append(face)
            else:
                named_frames[fnb] = [face]
    return named_frames

def remove_duplicate_names(named_frames, names):
    print("Cleaning")
    # removing duplicates
    print(names) # TODO WTF 3 3 1 1 2 4 3 ???
    cleaned_named_frames = {}
    for fnb, frame in progressbar(named_frames.items()):
        cleaned_named_frames[fnb]=list()
        for name in names:
            if name != 0:
                ordered = sorted([face for face in frame if face["name"]==name],
                              key=lambda f: f["conf"])
                if len(ordered) > 0:
                    #print(fnb, ordered)
                    best = ordered[-1]
                    cleaned_named_frames[fnb].append(best)
    return cleaned_named_frames

def add_missing_profiles(named_frames, autofill_names):
    #print(autofill_names)
    #pprint(named_frames[1][0])
    for fnb in range(1, 1 + max(named_frames.keys())): # add one
        for name in sorted(autofill_names):
            if fnb not in named_frames:
                named_frames[fnb] = list()
            if name is not '0' and \
               name not in [face["name"]
                            for face in named_frames[fnb]]:
                print(f"{fnb} : didnt find {name}")
                named_frames[fnb].append({
                    'box' : np.array([0. for _ in range(4)]),
                    'cen' : np.array([0. for _ in range(2)]),
                    'emb' : np.array([0. for _ in range(128)]),
                    'conf': 0,
                    'dist': 1,
                    'lms' : np.array([0. for _ in range(68*3)]).reshape((-1,3)),
                    'name': name,
                    })
    return named_frames

def full_tracker(tmpfile=".tmp/tmp", max_frame=None, embedding_tolerance=0.67,
                autofill_names=[]):
    faces = load_tmp(max_frame=max_frame)
    frames = group_faces_by_frames(faces=faces)
    if len(autofill_names) != len(frames[1]):
        print(f"Wrong number of targets specified!\n",
              f"Expected {len(frames[1])} but got {len(autofill_names)}\n",
              f"should be equal to the number of faces detected in ",
              f"the first frame!\n",
              f"specify the names with the -n option\n",
              f"run with --help for more infos",sep="")
        exit(1)

    tracked = spatio_temporal_tracking(frames=frames)
    FI = create_face_identifier_from_known_sessions(names=autofill_names,
                                                    frames=frames,
                                                    tracked=tracked)
    identified = identify_in_tracked_sessions(tracked, FI)
    named_frames = calculate_confidence_score(identified,
                                              embedding_tolerance=embedding_tolerance)
    named_frames = remove_duplicate_names(named_frames, names=autofill_names)
    return add_missing_profiles(named_frames, autofill_names)

if __name__ == "__main__":
    args=Parser()
    embedding_tolerance=0.67
    frames = full_tracker(max_frame=None, autofill_names=args.names,
                          embedding_tolerance=embedding_tolerance)

    print("Result")
    cam = cv2.VideoCapture("videos/00042.MTS")
    cv2.namedWindow("Face detected", cv2.WINDOW_NORMAL)
    cv2.setWindowProperty("Face detected", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    for fnb, frame in sorted(frames.items()):
        ret_val, img = cam.read()
        if fnb % 4 > 0:
            continue
        for face in frame:
            conf, name = face["conf"], face["name"]
            if name != "unknown":
                dist=(1-conf)
                for lm in face["lms"]:
                    cv2.circle(img, (int(lm[0]), int(lm[1])), 2, (255,255,255), 2, 1)
                x,y,w,h = int(face["box"][0]), int(face["box"][1]), int(face["box"][2]), int(face["box"][3])
                cv2.circle(img, (int(x+w/2),int(y+h/2)), 2, (0,0,255), 2, 1)
                if conf > embedding_tolerance:
                    cv2.rectangle(img, (x,y), (x+w, y+h), (0,int(conf*255),int(dist*255)), 4)
                    cv2.putText(img, f"{name}: {conf:.2f}", (x,y-3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2)
                else:
                    cv2.rectangle(img, (x,y), (x+w, y+h), (0,0,255), 4)
                    cv2.putText(img, f"{name}: {conf:.2f}", (x,y-3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2)
        cv2.imshow('Face detected', img)
        if cv2.waitKey(1) == 27:
            break  # esc to quit
    cv2.destroyAllWindows()

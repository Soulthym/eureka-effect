def resizeBox(x1,y1,x2,y2,size,minx,miny,maxx,maxy):
    xc = (x1+x2)/2
    yc = (y1+y2)/2
    return int(max(minx, size*(x1-xc)+xc)),\
           int(max(miny, size*(y1-yc)+yc)),\
           int(min(maxx, size*(x2-xc)+xc)),\
           int(min(maxy, size*(y2-yc)+yc))

if __name__ == "__main__":
    print(resizeBox(10,10,20,20,.5,0,0,200,200))

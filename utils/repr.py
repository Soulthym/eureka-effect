def get_time_repr(frame_number, framerate):
    frame_number = int(frame_number/framerate)
    s = frame_number%60
    frame_number = int(frame_number/60)
    m = frame_number%60
    frame_number = int(frame_number/60)
    h = frame_number
    return f"{h}:{m}:{s}"

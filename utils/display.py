import cv2

def draw_3d_landmarks(global_image, DDD_features):
    [cv2.circle(global_image, (int(lm[0]), int(lm[1])), 3, (0,200,0),2) for lm in DDD_features]

def draw_3d_orientation_vector(global_image, vector):
    cv2.line(global_image, (vector[0][0], vector[0][1]), (vector[1][0], vector[1][1]), (255,0,0), 1) 
    
def draw_face_box(global_image, top, bottom, left, right, name="") :
    """draw a box around a face"""
    cv2.putText(global_image, name, (left, top-5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,200,0), 2, cv2.LINE_AA)
    cv2.rectangle(global_image, (left, top), (right, bottom), (0,0,255), 2)
    

def draw_landmarks(global_image, faces_landmarks) :
    """draw the landmark list"""
    for label, points in faces_landmarks.items() :
        for point in points :
            cv2.line( global_image, point, point, (255, 255, 255), 4)
            

def draw_emotion(global_image, face_emotions) :
    """draw the amotion label"""
    pos = face_emotions['pos']
    cv2.putText(global_image, face_emotions['emo'], (pos[0], pos[1]+22), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,000,50), 2, cv2.LINE_AA  )

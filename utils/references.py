reference={ "top":0,
            "left":1,
            "bottom":2,
            "right":3,
            "centre_x":4,
	    "centre_y":5,
            "ref_z":6,
            "mvt_x":7,
            "mvt_y":8,
            "mvt_z":9,
            "landmarks":10,
            "is_speaking":11,
            "emotions":12,
        }

#Emotion
emotion_model_path = 'emotion_recognition/utils/models/fer2013_mini_XCEPTION.102-0.66.hdf5'

#PNR
PNR_model_path = 'DDD_extraction/Model_Data/256_256_resfcn256_weight'
PNR_uv_landmarks = 'DDD_extraction/Model_Data/uv_kpt_ind.txt'
PNR_uv_index =  'DDD_extraction/Model_Data/face_ind.txt'
PNR_vertices = 'DDD_extraction/Model_Data/canonical_vertices.npy'

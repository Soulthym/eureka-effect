import os
import shutil
import io
import numpy as np

class csv_writer():
    def __init__(self, path, namefile, keys, prefix=""):
        """ writes the first line of the csv file, based on dict keys """
        self.name = prefix + namefile.split('/')[-1].split(".")[0] + ".csv"
        if not os.path.exists(path):
            os.makedirs(path)
        with open(path+"/"+self.name, "w") as f:
            f.write(";".join(keys)+"\n")
        self.path = path
        self.keys = keys

    def append(self, data):
        """ appends a dict to a csv file, it needs to be initialized first with init_csv
            if the element is not a single int/str, it writes it to a npz file and write the name of the file in its place"""
        output = io.StringIO()
        for k in self.keys:
            if k not in data:
                output.write("None")
            elif isinstance(data[k], str):
                output.write(data[k])
            else:
                output.write(repr(data[k]))
                #np.savetxt(output, np.array(data[k]).reshape(-1), delimiter=',', newline=',', fmt='%g') 
            output.write(";")
        with open(self.path+"/"+self.name, "a") as f:
            f.write(output.getvalue()+'\n')
        output.close()

    def newSection(self, sectionName=""):
        with open(self.path+"/"+self.name, "a") as f:
            f.write(f'{sectionName}\n')


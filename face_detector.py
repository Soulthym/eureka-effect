#import emotion_classifier
import torch
import face_alignment
import numpy as np
import cv2
from pprint import pprint
import time
import face_recognition
from scipy import spatial

class FaceDetector():
    def __init__(self, device='cpu'):
        with torch.no_grad():
            self.face_al = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, flip_input=False, device=device, face_detector='sfd')

    def detect(self, img):
        if isinstance(img,np.ndarray):
            all_landmarks = self.face_al.get_landmarks_from_image(img)
            if not isinstance(all_landmarks, list):
                all_landmarks = []
            faces = [{"faceLandmarks":lms} for lms in all_landmarks]
            for face in faces:
                face["faceRectangle"]={}
                x,y=np.amin(face["faceLandmarks"],axis=0)[:-1]
                x2,y2=np.amax(face["faceLandmarks"],axis=0)[:-1]
                x,y,w,h = int(x), int(y), int(x2-x), int(y2-y)
                x,y,w,h = (x if x >= 0 else 0,
                           y if y >= 0 else 0,
                           w if x+w <= img.shape[1] else img.shape[1]-x,
                           h if y+h <= img.shape[0] else img.shape[0]-y)
                face["faceRectangle"]['left'], \
                face["faceRectangle"]['top'], \
                face["faceRectangle"]['width'], \
                face["faceRectangle"]['height'] = x,y,w,h
                face["faceLandmarks"] = {str(i):{k:v for k,v in zip("xyz",lm)} for i,lm in enumerate(face["faceLandmarks"])}
                x=face["faceRectangle"]['left']
                y=face["faceRectangle"]['top']
                w=face["faceRectangle"]['width']
                h=face["faceRectangle"]['height']
                #face["emotions"] = "str(emotion_classifier.get_emotion(img[y:y+h,x:x+w]))"
            embeddings = face_recognition.face_encodings(img,
                                                         known_face_locations=[
                        (f["faceRectangle"]['top'],                                 # Top
                         f["faceRectangle"]['left']+f["faceRectangle"]['width'],    # Right
                         f["faceRectangle"]['top']+f["faceRectangle"]['height'],    # Bottom
                         f["faceRectangle"]['left'])                                # Left
                                for f in faces],
                                                         num_jitters=100)
            for f,e in zip(faces,embeddings):
                f["faceEmbeddings"] = e
            self.result = faces
            return self.result
        else:
            return []

class FaceIdentifier():
    def __init__(self):
        self.IDs = dict()
        self.person_number=0

    def newPerson(self, name=None):
        """
        Initializes a new entry to the database of known persons.
        returns the number of the person's profile
        Parameters:
            name:
                person's name
                if not specified, it will use an incremental value.
                Use the same interface regardless of the current Instance to identify people.
                It does not create profiles in all of the instances of FaceDetector, only this local one...
                #TODO : Improve that by spliting this up into 2 modules...
        """
        if not name:
            name = self.person_number
        self.IDs[name] = list()
        self.person_number += 1
        return self.person_number - 1

    def train(self, name, ID):
        """
        Adds a new entry to the person's profile.
        Parameters:
            name:
                name of the person whose ID is being saved
            ID:
                ID to save in ther person's profile
        !!! Once every face is added, you need to build the database for query.
            This is done using the buildDB method.
        """
        if type(ID) is np.ndarray:
            if name in self.IDs:
                self.IDs[name].append(ID)

    def buildDB(self):
        """
        Creates the database for requesting IDs.
        Uses scikit's KDTree structure.
        Run after training to submit your new profiles to the database.
        Run BEFORE indentifying people, as it requires the database to perform queries...
        """
        d = list(zip(*[(name,ID) for name in self.IDs.keys() for ID in self.IDs[name]]))
        self.name_list = d[0]
        self.ID_list = d[1]
        self.db = spatial.cKDTree(self.ID_list,
                                  leafsize=128,
                                  compact_nodes=True,
                                  balanced_tree=True)

    def identify(self, ID):
        """
        Identifies a person given their ID/image
        returns their the cosine distance to the closest name along with the corresponding name, the whole as a tuple
        Parameters:
            ID:
                face Embedding of the person to identify
        """
        if type(ID) is np.ndarray:
            dist, idx = self.db.query(ID)
            return dist, self.name_list[idx]

def show(FD, img):
    for face in FD.result[:]:
        if 'faceRectangle' in face:
            x,y,w,h = [face['faceRectangle'][i] for i in ['left', 'top', 'width', 'height']]
            cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0),4,8)
        if 'faceLandmarks' in face:
            for name, lm in face["faceLandmarks"].items():
                cv2.circle(img,(lm["x"],lm["y"]), 1, (220,0,180), -1)

if __name__ == "__main__":
    screen_width = 800
    screen_height = 600
    FD = FaceDetector()
    FI = FaceIdentifier()
    FI.newPerson("Thy")
    cap = cv2.VideoCapture(0)
    print("=== Training ===")
    while 1:
        ret, img = cap.read() 
        result = FD.detect(img=img)
        for face in result:
            print(face["faceEmbeddings"])
        #ID = result[0]["faceEmbeddings"]
        # Add an example to the DB of known faces
        #FI.train(name="Thy", ID=ID)
        show(FD,img)
        cv2.imshow('YOLO Face detection',img)
        if cv2.waitKey(1) & 0xff == 27:
            break
    #print("IDs:")
    #for k in FI.IDs:
    #    print(f"{k}:")
    #    for v in FI.IDs[k]:
    #        print(f"\t{v}")
    #print("zip")
    ## Commit the profiles to the database
    #FI.buildDB()
    #print("=== Analyzing ===")
    #while 1:
    #    ret, img = cap.read() 
    #    img = cv2.resize(img, (screen_width, screen_height))
    #    res = FD.detect(img=img)
    #    IDs = [face["faceEmbeddings"] for face in res]
    #    names = [FI.identify(ID=ID) for ID in IDs]
    #    print(names)
    #    show(FD,img)
    #    cv2.imshow('YOLO Face detection',img)
    #    if cv2.waitKey(1) & 0xff == 27:
    #        break
    #cap.release()
    #cv2.destroyAllWindows() 

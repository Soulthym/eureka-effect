# -*- coding: utf-8 -*-
import pickle
import speaking
import dyades
from load import format
import numpy as np
from io import StringIO
from pprint import pprint
import os

def chargementPickles(name):
    res = format(stride=12, window=12,
                 data_file=f"./out/data-{name}.csv",
                 #speaking_file="./3v2FR/Speaking_Dyades_3v2FR.csv",
                 keys=("landmarks", "centre", "vector3D"),
                 key_funcs={"landmarks":
                     lambda x: np.loadtxt(StringIO(x.strip()),
                                          delimiter=','
                                         ).reshape((68,3))[48:,:2].reshape((-1))}
                )
    lms = res["landmarks"]
    ctr = res["centre"]
    vec = res["vector3D"]
    return lms, vec, ctr

def process(name):
    print("=== LOADING DATA ===")
    lms, vec, ctr = chargementPickles(name)
    print("=== PROCESSING SPEAKING ===")
    dico_prediction = speaking.extractor_processing(lms)
    with open(f"./out/{name}-speaking_PRED.pickle", 'wb') as handle:
        pickle.dump(dico_prediction, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("=== PROCESSING DYADE ===")
    predictions = dyades.extractor_processing(name, vec, ctr, dico_prediction)
    with open(f"./out/{name}-dyade_PRED.pickle", 'wb') as handle:
        pickle.dump(predictions, handle, protocol=pickle.HIGHEST_PROTOCOL)
    for k,v in predictions.items():
        print(k, type(v), len(v))

    with open(f".tmp/data-{name}.csv", 'w+') as outf:
        with open(f"./out/data-{name}.csv", 'r') as f:
            header = f.readline().strip()
            outf.write(header+"\n")
            header = header.split(';')
            for l in f.readlines():
                l = {k:v for k,v in zip(header, l.strip().split(';'))}
                sec = sum(
                        int(x)*y for x,y in zip(l["time"].split(":"),
                                           [360,60,1]))
                l["is_speaking"] = predictions[l["name"]][sec] \
                    if sec < len(predictions[l["name"]]) else 0
                outf.write(";".join(str(l[k]) for k in header)+"\n")
        os.remove(f"./out/data-{name}.csv")
    os.rename(f".tmp/data-{name}.csv", f"./out/data-{name}.csv")
    print("Done")

if __name__ == "__main__":
    #name = '20180417_rencontrecam_3_v2_france_lang'
    name = '00042'
    process(name)

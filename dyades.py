# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 16:18:05 2019

@author: piotm
"""
import pickle
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D, proj3d
import matplotlib.patches as mp
import statistics
import matrice_confusion as mc
import os

class Arrow3D(mp.FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        mp.FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        mp.FancyArrowPatch.draw(self, renderer)


def show3D (visages, name):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(0, 1440)
    ax.set_ylim(0, 1080)
    ax.set_zlim(0, 1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.title('Video '+name+' - Vector representation \n Frame 1 (Reference)')
    ax.view_init(45, -90)
    for i, visage in enumerate(visages, 1):
        a = Arrow3D(visage[0],visage[1],visage[2], mutation_scale=20, lw=1,
                    arrowstyle="-|>", color="k")
        ax.add_artist(a)
        ax.text(visage[0][0], visage[1][0], 0, str(i))
    plt.show()

def show2D(visages, name):
    ax=plt.axes()
    ax.set_xlim(-0.2, 0.2)
    ax.set_ylim(0, 5)
    ax.set_xlabel("Vector length")
    ax.set_ylabel("Subject")
    plt.suptitle('Video '+name+' - Vector length')
    plt.title('Frame 1 (Reference)')
    for i,visage in enumerate(visages, 1):
        ax.arrow(0,i,visage, 0,
                 head_width=0.1, head_length=0.01, fc='k', ec='k')
    plt.show()

def check(predictions, label, key, name):
    rate=0
    #pred,lab : 0=0, 0=1, 0=2, 1=0, 1=1, 1=2, 2=0, 2=1, 2=2
    confusion = np.zeros((3,3), dtype=np.double)
    for p, lab in zip(predictions, label):
        confusion[int(p)][lab[0]]+=1
    for r in range (0, confusion.shape[0]):
        rate+=confusion[r][r]
    rate = rate/len(label)


    pourcentage_conf=np.copy(confusion)
    for i in range(0,pourcentage_conf.shape[0]):
        summ = sum(pourcentage_conf[:, i])
        for j in range(0,pourcentage_conf.shape[1]) :
            pourcentage_conf[j][i]=pourcentage_conf[j][i]/summ
    pourcentage_conf=(pourcentage_conf)*100

    fig, ax = plt.subplots()
    im, cbar = mc.heatmap(pourcentage_conf, ['No \n speaking','Intergroup \n speaking', 'Intragroup \n speaking'],
                           ['No \n speaking','Intergroup \n speaking', 'Intragroup \n speaking'], key, ax=ax,
                   cmap="YlGn", cbarlabel="% of dyad identification")
    texts = mc.annotate_heatmap(im, valfmt="{x:.2f} %", data2=confusion)
    fig.tight_layout()
    plt.suptitle("Confusion matrix - Subject %s - Video %s"%(key,name), y=1)
    plt.show()
    return rate

def refLong(vec, ctr):
    references={}
    for key, vecteurs, centres in zip(vec.keys(), vec.values(),
                                       ctr.values()):
        for timeV, timeC in zip(vecteurs, centres) :
            for secondeV, secondeC in zip(timeV, timeC) :
                secondeC=np.append(secondeC, 0)
                depart=np.array(secondeV[0]+secondeC)
                arrivee=np.array(secondeV[1]+secondeC)
                references[key]=arrivee[0]-depart[0]
                break
            break
    return references, vecteurs.shape[0]

def calcOrientationsStat(vec, ctr, references):
    orientations = {}
    for key, vecteurs, centres, ref in zip(vec.keys(), vec.values(),
                                       ctr.values(), references.values()):
        meanproj = []
        for timeV, timeC in zip(vecteurs, centres) :
            projections=[]
            for secondeV, secondeC in zip(timeV, timeC) :
                secondeC=np.append(secondeC, 0)
                depart=np.array(secondeV[0]+secondeC)
                arrivee=np.array(secondeV[1]+secondeC)
                long = arrivee[0]-depart[0]
                projections.append(long-ref)

            meanproj.append(statistics.mean(projections))
        orientations[key]=meanproj
    return orientations

def deterCorrelation(orientations, spk):
    correlations={}
    for key, val, speaking in zip(orientations.keys(),
                                      orientations.values(), spk.values()):
        orientation=np.array(val)
        correlation= orientation*speaking
        correlations[key]=correlation
    return correlations


def calcStat(correlations, time):
    moyennes={}
    for key, valeur in correlations.items():
        pos=[]
        neg=[]
        for v in valeur :
            if v != 0 :
                if v >0:
                    pos.append(v)

                else :
                    neg.append(v)

        if pos == []:
            pos.append(0)
        if neg == []:
            neg.append(0)
        moyennes[key]=[np.full_like(time,statistics.mean(pos), dtype=np.double),
               np.full_like(time,statistics.mean(neg), dtype=np.double)]

    return moyennes

def smoothingPredictions (predictions):
    for key, p in predictions.items():
        cpt = 0
        while cpt < len(p):
            deb = cpt
            intra=0
            inter=0
            while ((cpt < len(p)) and (p[cpt]!= 0)):
                if p[cpt]==1:
                    inter+=1
                else :
                    intra+=1
                cpt+=1
                fin=cpt
            if intra != 0 or inter != 0 :
                if intra>=inter :
                    for i in range(deb, fin):
                        p[i]=2
                else :
                    for i in range(deb, fin):
                        p[i]=1
            cpt+=1
        return predictions

def classi(correlations, moyennes):
    predictions={}
    for key, corr, m in zip(correlations.keys(), correlations.values(),
                            moyennes.values()):
        pred=[]
        PM=m[0][0]
        NM=m[1][0]
        for c in corr :
            if c != 0 :
                if (PM>=c>=NM) == True :
                    pred.append(1)
                elif c>PM or c<NM :
                    pred.append(2)
            else :
                pred.append(0)
        predictions[key]=pred
    return predictions
def graphData(dlb, correlations, moyennes, references, time, name):
    for key, ll, val, m , r in zip(correlations.keys(), dlb.values(),
                           correlations.values(), moyennes.values(),
                           references.values()):
        color=[]
        for l in ll:
            if l[0] == 0 :
                color.append('grey')
            elif l[0] == 1 :
                color.append('orange')
            else :
                color.append('red')

        plt.scatter(time, val, marker='+', color=color)
        g = mp.Patch(color='grey', label ="Isn't speaking" )
        o = mp.Patch(color='orange', label = "Inter group dyad" )
        red = mp.Patch(color='red', label = "Intra group dyad" )
        b = mp.Patch(color='blue', label = "Positive and Negatie mean" )
        bl = mp.Patch(color='black', label="Reference (orientation on frame 1)" )
        plt.suptitle("Evolution of the length of orientation vectors over time")
        plt.title("Subject %s - Video %s"%(key, name), fontsize=10)
        plt.xlabel('Time (s)')
        plt.ylabel("Vector's length")
        plt.plot(time, m[0], color='b')
        plt.plot(time, m[1], color='b')
        plt.plot(time, list(np.full_like(time, r-r, dtype=np.double)), color='black')
        plt.legend(handles=[g, o, red, b, bl], ncol=2, mode="expand", bbox_to_anchor=(0.65, -0.15), borderaxespad=0.0)
        if not os.path.exists("./"+name+"/Resultats dyade/"):
            os.makedirs("./"+name+"/Resultats dyade/")
        plt.savefig("./"+name+"/Resultats dyade/Subject %s - Vector's length (recentre).png"%key)
        plt.show()

def graphResultats(rates, ref, name):
    colorbar=[]
    rates=np.array(rates)*100
    rates=np.append(rates, sum(rates)/rates.size)
    keys=list(ref.keys())
    keys.append("Global")
    for i in range(len(keys)):
        if i != len(keys)-1 :
            colorbar.append('b')
        else :
            colorbar.append('orange')
    plt.bar(keys,rates, width=0.5, color = colorbar)
    for i in range(len(keys)):
        plt.text(x = i-0.25 , y = rates[i]+1, s =f"{rates[i]:.2f}%", size = 11)
    plt.xlabel('Subject')
    plt.ylabel('Inference (%)')
    plt.suptitle("Inference on dyad identification")
    plt.title("Video"+name, fontsize = 10)
    plt.show()


def extractor_processing(name, vec, ctr, spk):
    rates=[]

    references, duration = refLong(vec, ctr)
    time=np.arange(1, duration+1)
    orientations = calcOrientationsStat(vec, ctr, references)
    correlations = deterCorrelation(orientations, spk)
    moyennes = calcStat(correlations, time)

    predictions = classi(correlations, moyennes)
    predictions = smoothingPredictions(predictions)

    #graphData(dlb, correlations, moyennes, references, time, name)

    #for key, prediction in zip(predictions.keys(), predictions.values(),
    #                                dlb.values()):
    #    rate=check(prediction, lbl, key, name)
    #    rates.append(rate)
    #graphResultats(rates, references, name)
    return predictions

def processing(name, vec, dlb, ctr, spk):
    rates=[]

    references, duration = refLong(vec, ctr)
    time=np.arange(1, duration+1)
    orientations = calcOrientationsStat(vec, ctr, references)
    correlations = deterCorrelation(orientations, spk)
    moyennes = calcStat(correlations, time)

    predictions = classi(correlations, moyennes)
    predictions = smoothingPredictions(predictions)

    graphData(dlb, correlations, moyennes, references, time, name)

    for key, prediction, lbl in zip(predictions.keys(), predictions.values(),
                                    dlb.values()):
        rate=check(prediction, lbl, key, name)
        rates.append(rate)

#    with open('./'+name+'/dyade_PRED.pickle', 'wb') as handle:
#        pickle.dump(predictions, handle, protocol=pickle.HIGHEST_PROTOCOL)
    graphResultats(rates, references, name)

# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 15:16:51 2019

@author: melanie piot && soulthym
"""
import sys
from progressbar import progressbar
import cv2
import sys
import os

class Video():
    def __init__(self, path, outpath=".tmp/", start_frame=0):
        self.path = path
        self.outpath = outpath
        self.start_frame = start_frame
        self.stream = cv2.VideoCapture(path)
        if not self.stream.isOpened() :
            #Cannot open video capture
            print( "Cannot open source video file at location : " + str(path))
            sys.exit()

        self.frame=None
        self.num_frame=0
        self.size = (int(self.stream.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        self.fps = self.stream.get(cv2.CAP_PROP_FPS)
        if self.fps == 0.0:
            self.fps = 24
        print(f"Starting at frame {start_frame}")
        for _ in progressbar(range(start_frame)):
            self.num_frame += 1
            self.stream.read()
        videoName = str(path).split("/")[-1].split(".")
        print(videoName)
        if len(videoName) > 1:
            videoName = ".".join(videoName[:-1])
        else:
            videoName = ".".join(videoName)
        if not os.path.exists(outpath):
            os.mkdir(outpath)
        outname = str(outpath)+"/out_"+videoName+".avi"
        print(f"saving as : {outname} || FPS : {self.fps} || Size : {self.size}")
        self.writer = cv2.VideoWriter(outname, cv2.VideoWriter_fourcc(*'MJPG'),
                                      self.fps, self.size, True)
        #self.read()

    def reload(self):
        self.stream.release()
        self.__init__(path=self.path, outpath=self.path,
                      start_frame=self.start_frame)

    def writeFrame(self):
        self.writer.write(self.frame)

    def decoupageVisage(self, top, left, bottom, right): # decoupe les visages de la frame
        face_image = self.frame[top:bottom, left:right]
        return face_image

    def read(self):
        (grabbed, frame) = self.stream.read()
        if not grabbed:
            return None
        self.frame = frame
        self.num_frame+=1
        return self.frame

#    def affichage(self, sujets):
#        """Affichage des différents elements de chaque sujet"""
#        for sujet in sujets:
#            if sujet.name:
#                # affichage box
#                cv2.rectangle(self.frame, (sujet.box[1],sujet.box[0]), (sujet.box[3],sujet.box[2]),
#                              (0,255,0),4,8)
#                cv2.circle(self.frame,(sujet.center[0],sujet.center[1]), 1, (0,180,180), -1)
#                # affichage landmarks
#                for nb, landmark in sujet.landmarks.items():
#                    x,y = (landmark['x'], landmark['y'])
#                    if 0 <= x <= self.stream.get(cv2.CAP_PROP_FRAME_WIDTH) and 0 <= y <= self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT):
#                        cv2.circle(self.frame,(x,y), 1, (220,0,180), -1)
#                        #cv2.putText(self.frame, str(nb), (x,y), cv2.FONT_HERSHEY_SIMPLEX, .3, (255,255,255), 1)
#
#                #affichage nom
#                cv2.putText(self.frame, sujet.name, (sujet.box[1],sujet.box[0]), cv2.FONT_HERSHEY_SIMPLEX, .5, (255,255,255), 1)
#
#                #affichage emotion
#
#                #affichage vecteur
#                cv2.line(self.frame,
#                        (sujet.face_center[0],                     sujet.face_center[1]),
#                        (sujet.face_center[0] + sujet.vector3D[0], sujet.face_center[1] + sujet.vector3D[1]),
#                        (255,0,0), 2)
#

if __name__ == "__main__" :
    try:
        video = sys.argv[1]
    except IndexError:
        video = "./videos/00042.MTS"
    root = ".".join(video.split("/")[-1].split(".")[:-1])
    datafile = f"out/data-{root}.csv"
    print(datafile)
    v = Video(video)
    img = v.read()
    frame = 1
    with open(datafile, "r") as df:
        keys = df.readline().strip().split(";")
        for l in df.readlines():
            face = {k:v for k,v in zip(keys, l.strip().split(";"))}
            while frame > v.num_frame:
                cv2.imshow("win", img)
                img = v.read()
                if img is None or cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            if frame == v.num_frame:
                pass
            frame = int(face["frame"])
    cv2.destroyAllWindows()
    v.stream.release()

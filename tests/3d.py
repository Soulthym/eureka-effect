from sympy.geometry import Point, Line
import json
from pprint import pprint
import numpy as np
from math import sqrt
import cv2

def fixlandmark68(landmarks):
    p1 = Point(landmarks["63"]["x"], landmarks["63"]["y"])
    p2 = Point(landmarks["67"]["x"], landmarks["67"]["y"])
    p3 = Point(landmarks["66"]["x"], landmarks["66"]["y"])
    x,y = p3.reflect(Line(p1,p2)).evalf()
    landmarks["68"]["x"], landmarks["68"]["y"] = int(x),int(y)
    return landmarks

faces = []
with open("landmarks",'r') as f:
    for l in f.readlines():
        faces.append(json.loads(l.strip()))

window="face"
cv2.namedWindow(window, cv2.WINDOW_NORMAL)
cv2.setWindowProperty(window, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
image = np.zeros((600,800))
for i in range(6):
    cv2.imshow(window, image)
    cv2.waitKey(1)

speed = 200
for i, face in enumerate(faces):
    image = np.zeros((600,800,3))
    
    for name,lm in fixlandmark68(face).items():
        if name != "68":
            image[lm["y"],lm["x"]] = (255,255,255)
        elif 0<=lm["x"]<800 and 0<=lm["y"]<600:
            image[lm["y"],lm["x"]] = (0,0,255)
        cv2.putText(image, name, (lm["x"],lm["y"]), cv2.FONT_HERSHEY_SIMPLEX, .2, (255,255,255), 1)
    cv2.putText(image, str(i), (0,10), cv2.FONT_HERSHEY_SIMPLEX, .5, (255,255,255), 1)
    cv2.imshow(window, image)
    
    if cv2.waitKey(speed) & 0xff == 27:
        break

cv2.destroyAllWindows()
#       63
#             66
#?68
#        67

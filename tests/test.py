from functools import partial
import sys
import threading
import cv2 
import requests
import argparse 
import os
from pprint import pprint
import subprocess
import psutil
import time
from scipy import spatial

class FaceDetector():
    def __init__(self, new=True, ip="127.0.0.1", port="5000", opt= { "detector":"yolo", 
                                                           "dblScale":"0", 
                                                           "expandBy":"0.1", 
                                                           "returnFaceAttributes":"true", 
                                                           "returnFaceId":"true", 
                                                           "returnFaceLandmarks":"true",},
                                                        ):
        """
        A Face Detector and Facial information retriever based on Deepsight's implementation
            parameters: 
                new=True : 
                    If set to True it will kill any instance of the Deepsight server running on the same port as specidied. If not it will try to connect to the server specified by ip and port.
                ip="127.0.0.1" :
                    The ip of the Deepsight server
                port="5000":
                    The port used by the Deepsight server
                opt= { "detector":"yolo", 
                       "dblScale":"0", 
                       "expandBy":"0.1", 
                       "returnFaceAttributes":"true", 
                       "returnFaceId":"true", 
                       "returnFaceLandmarks":"true"}:
                       The options of the Deepsight request. See Deepsight's Documentation for further information
        """
        self.ip=ip
        self.port=port
        self.opt=opt
        self.api = f"http://{ip}:{port}/inferImage?"+"&".join([k.strip()+"="+v.strip() for k,v in opt.items()])
        self.IDs = dict()
        self.person_number=0
        print("using "+self.api)
        cli=["../Deepsight/bin/dsFace", "-p", port]
        if new and ip == "127.0.0.1":
            print(f"killing {cli}")
            for proc in psutil.process_iter():
                if proc.cmdline() == cli:
                    proc.kill()
            self.server=subprocess.Popen(cli)
        else:
            self.server=None

    def detect(self, img):
        """
        Sends a request to the Deepsight server to get informations about an image.
        returns the json dump of the result of the request. 
            list of elements, each describing informations about a person, except the last element.
            So to get the list of facial informations, use as such: 
                FaceDetector.detect()[:-1]
        Parameters:
            img: cv2 image, often compatible with 2D/3D(color) np.array
        """
        r, imgbuf= cv2.imencode(".bmp", img)
        r = requests.post(self.api, files={'pic':bytearray(imgbuf)})
        self.result = r.json()
        return self.result

    def newPerson(self, name=None):
        """
        Initializes a new entry to the database of known persons.
        returns the number of the person's profile
        Parameters:
            name:
                person's name
                if not specified, it will use an incremental value.
                Use the same interface regardless of the current Instance to identify people.
                It does not create profiles in all of the instances of FaceDetector, only this local one...
                #TODO : Improve that by spliting this up into 2 modules...
        """
        if not name:
            name = self.person_number
        self.IDs[name] = list()
        self.person_number += 1
        return self.person_number - 1

    def train(self, name, img=None, ID=None):
        """
        Adds a new entry to the person's profile.
        Parameters:
            name:
                name of the person whose ID is being saved
            ID:
                ID to save in ther person's profile
            img:
                If ID isn't specified, you can use this method
                image containing ONLY the person's face. It can crash if there is more than one face detected!
                sends a request to the server to get the ID of the person.
        !!! Once every face is added, you need to build the database for query.
            This is done using the buildDB method.
        """
        if ID:
            if name in self.IDs:
                self.IDs[name].append(ID)
        elif img:
            result = self.detect(img)
            if len(result) == 2 and name in self.IDs:
                self.IDs[name] = result[0]["faceEmbeddings"]

    def buildDB(self):
        """
        Creates the database for requesting IDs.
        Uses scikit's KDTree structure.
        Run after training to submit your new profiles to the database.
        Run BEFORE indentifying people, as it requires the database to perform queries...
        """
        d = list(zip(*[(name,ID) for name in self.IDs.keys() for ID in self.IDs[name]]))
        self.name_list = d[0]
        self.ID_list = d[1]
        self.db = spatial.KDTree(self.ID_list)

    def identify(self, img=None, ID=None):
        """
        Identifies a person given their ID/image
        returns their name
        Parameters:
            ID:
                face Embedding of the person to identify
            img:
                If ID isn't specified, you can use this method
                image containing ONLY the person's face. It can crash if there is more than one face detected!
                sends a request to the server to get the ID of the person, then identifies the person
        """
        if ID:
            dist, idx = self.db.query(ID)
            return dist, self.name_list[idx]
        elif img:
            result = self.detect(img)
            if len(result) == 2 :
                dist, idx = self.db.query(result[0]["faceEmbeddings"])
                return dist, self.name_list[idx]

    def show(self, img):
        for face in self.result[:-1]:
            if 'faceRectangle' in face:
                x,y,w,h, confidence = [face['faceRectangle'][i] for i in ['left', 'top', 'width', 'height', 'confidence']]
                cv2.rectangle(img, (x,y), (x+w,y+h), (0,confidence*255,(1-confidence)*255),4,8)
            if 'faceLandmarks' in face:
                lms = [lm for lm in face["faceLandmarks"].values() if 0<=lm["x"]<=screen_width and 0<=lm["y"]<=screen_height]
                for lm in lms:
                    cv2.circle(img,(lm["x"],lm["y"]), 1, (220,0,180), -1)

def func(i, FD):
    with camlock:
        ret, img = cap.read() 
    img = cv2.resize(img, (screen_width, screen_height))
    FD.detect(img=img)
    imgs[str(i)] = img

camlock = threading.Lock()
nb_thread = int(sys.argv[1])
imgs = dict()
screen_width = 800
screen_height = 600
FDs = [FaceDetector(port=f"{5000+i}") for i in range(nb_thread)]
time.sleep(10)
cap = cv2.VideoCapture("vid.avi")
for i in range(5):
    t = time.time()
    Ts = [threading.Thread(name=f'server {5000+i}', target=partial(func, i, FD)) for i, FD in enumerate(FDs)]
    for T in Ts:
        T.start()
    for T in Ts:
        T.join()
    for i, FD in enumerate(FDs):
        img = imgs[str(i)]
        FD.show(img)
        cv2.imshow('YOLO Face detection',img)
    t = time.time()-t
    print(f"Num Threads: {nb_thread}\tTime elapsed: {t:.2f}s\tTime /image: {t/nb_thread:.2f}s")
    #for face in res[:-1]:
    #    print(face["faceLandmarks"])
    if cv2.waitKey(1) & 0xff == 27:
        break
cap.release()
cv2.destroyAllWindows() 
for port in range(10):
    cli=["../Deepsight/bin/dsFace", "-p", str(5000+port)]
    print(f"killing {' '.join(cli)}")
    for proc in psutil.process_iter():
        if proc.cmdline() == cli:
            proc.kill()

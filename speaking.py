# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 13:14:06 2019

@author: piotm
"""
import matrice_confusion as mc
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import statistics
import numpy as np
from scipy import ndimage
import pickle

def check(predictions, label, key, name):
    #confusion=[0,0,0,0] #pred,lab : 0=0, 0=1, 1=0, 1=1
    confusion = np.zeros((2,2), dtype=np.double)
    rate=0
    for p, lab in zip(predictions, label):
        confusion[int(p)][lab]+=1
    for r in range (0, confusion.shape[0]):
        rate+=confusion[r][r]
    rate = rate/len(label)

    pourcentage_conf=np.copy(confusion)
    for i in range(0,pourcentage_conf.shape[0]):
        summ = sum(pourcentage_conf[:, i])
        for j in range(0,pourcentage_conf.shape[1]) :
            pourcentage_conf[j][i]=pourcentage_conf[j][i]/summ
    pourcentage_conf=(pourcentage_conf)*100
#    for p, lab in zip(predictions, label):
#        if p == 1 and lab == 1:
#            confusion[3]+=1
#        elif p == 0 and lab == 0 :
#            confusion[0]+=1
#        elif p == 1 and lab == 0 :
#            confusion[2]+=1
#        elif p == 0 and lab == 1 :
#            confusion[1]+=1
#    rate = (confusion[0]+confusion[3])/len(label)
#
#    pourcentage_conf=[[0,0],[0,0]]
#    pourcentage_conf[0][0]=confusion[0]/(confusion[0]+confusion[2])
#    pourcentage_conf[1][0]=confusion[2]/(confusion[0]+confusion[2])
#    pourcentage_conf[1][1]=confusion[3]/(confusion[1]+confusion[3])
#    pourcentage_conf[0][1]=confusion[1]/(confusion[1]+confusion[3])
#    pourcentage_conf=np.array(pourcentage_conf)*100

    fig, ax = plt.subplots()
    im, cbar = mc.heatmap(pourcentage_conf, ['No speaking','Speaking'], ['No speaking','Speaking'], key, ax=ax,
                   cmap="YlGn", cbarlabel="% of detection on is speaking")
    texts = mc.annotate_heatmap(im, valfmt="{x:.2f} %", data2=confusion)
    fig.tight_layout()
    plt.suptitle("Confusion matrix - Subject %s - Video %s"%(key,name), y=1)
    plt.show()

    return confusion, rate

def graphVitesses(x, y, res, pred, key, name):
    plt.plot(x, y, label="Speed")
    plt.plot(x, res, 'r', label="Smoothed speed")
    plt.plot(x,pred, 'g', label="Average speed")
    plt.legend()
    plt.title("Video "+ name +"- Subject %s"%key, fontsize=10)
    plt.suptitle("Analysis of the speed of movement between landmarks 62 and 66")
    plt.axis([0, 771, 0, 0.03])
    plt.xlabel('Time (s)')
    plt.ylabel('Speed')
    plt.show()
#
#    for l in lab :
#        if l == 1 :
#            couleur.append('r')
#
#        else :
#            couleur.append('b')
#
#    plt.scatter(x, res, c=couleur, marker ='+')
#    red = mp.Patch(color='r', label = "Is speaking")
#    blue = mp.Patch(color='b', label = "Isn't speaking")
#    plt.legend(handles=[red, blue])
#    plt.axis([0, 771, 0, 0.03])
#    plt.xlabel('Time (s)')
#    plt.ylabel('Smoothed speed')
#    plt.suptitle("Labeled average speed")
#    plt.title("Video 00042 - Subject %s"%key, fontsize=10)
#    plt.show()

def graphResultats(rates, lms, slb, colorbar, x, predictions, name):
    rates=np.array(rates)*100
    rates=np.append(rates, sum(rates)/rates.size)
    keys=list(lms.keys())
    keys.append("Global")
    for i in range(len(keys)):
        if i != len(keys)-1 :
            colorbar.append('b')
        else :
            colorbar.append('orange')
    plt.bar(keys,rates, width=0.5, color = colorbar)
    for i in range(len(keys)):
        plt.text(x = i-0.25 , y = rates[i]+1, s =f"{rates[i]:.2f}%", size = 11)
    plt.xlabel('Subject')
    plt.ylabel('Inference (%)')
    plt.suptitle("Inference on speaking detection")
    plt.title("Video"+name, fontsize = 10)
    plt.show()

    fig = plt.figure(figsize = (9,5))
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    fig.suptitle("Video"+ name +"- Speaking graph", fontsize = 14)
    ax1.set_title("Inference", fontsize=12)
    for sujet in predictions :
        ax1.scatter(x, sujet, c='b', marker = '_')
        ax1.axis([0, 771, 0.5, 4.5])
    ax1.set_ylabel("Subject")



    ax2.set_title("Label", fontsize=12)
    for cle, sujet in slb.items():
        tmp=[]
        for t in sujet :
            tmp.append(t[0]*int(cle))
        ax2.scatter(x, tmp, c='b', marker = '_')
        ax2.axis([0, 771, 0.5, 4.5])
    ax2.set_xlabel("Time (s)")
    ax2.set_ylabel("Subject")

    fig.subplots_adjust(hspace=0.5)

    plt.show()

def extractor_processing(lms) :
    rates=[]
    predictions=[]
    dico_prediction={}
    # parcours landmarks et label pour chaque personne
    for key, valeur in zip(lms.keys(), lms.values()):
        x=[]
        y=[]
        lab=[]
        colorbar=[]
        pred=[]
        tab=[]
        res=[]
        prediction=[]
        s = 0
        # calcule de la vitesse de déplacement frame 2 a 2 pour chaque sequence de 12
        for seq in valeur:
            vecteur0 = seq[0][9]-seq[0][37]
            for i in range(2, seq.shape[0]-1, 2):
                vecteur = seq[i][9]-seq[i][37]
                tab.append(abs(vecteur-vecteur0)/2)
                vecteur0=vecteur
            #calcul vitesse moyenne pour 12 frame (=1 seconde)
            tab.append(statistics.mean(tab))
            y.append(tab[-1])
            x.append(s)
            tab=[]

            s+=1

        # calcul vitesse moyenne / personne pour la video entière
        for i in x :
            pred.append(statistics.mean(y))
        # calcul moyenne glissante sur 6 sec
        taille = 6
        mat=np.array([1/taille]*taille)
        res=np.convolve(y, mat, 'valid')
        res=np.append([res[0]]*int(taille/2), res)
        res=np.append(res, [res[-1]]*int((taille/2)-1))

        # si moyenne glissante >= moyenne video ==> 1
        for p, ref in zip(res, pred):
            if p>=ref :
                prediction.append(1)
            else :
                prediction.append(0)
        prediction = np.array(prediction)
        prediction = ndimage.binary_erosion(prediction).astype(prediction.dtype)

        dico_prediction[key]=prediction
    return dico_prediction

def processing(lms, slb, name) :
    rates=[]
    predictions=[]
    dico_prediction={}
    # parcours landmarks et label pour chaque personne
    for key, valeur, val in zip(lms.keys(), lms.values(), slb.values()):
        x=[]
        y=[]
        lab=[]
        colorbar=[]
        pred=[]
        tab=[]
        res=[]
        prediction=[]
        s = 0
        # calcule de la vitesse de déplacement frame 2 a 2 pour chaque sequence de 12
        for seq, slbl in zip(valeur, val) :
            vecteur0 = seq[0][9]-seq[0][37]
            for i in range(2, seq.shape[0]-1, 2):
                vecteur = seq[i][9]-seq[i][37]
                tab.append(abs(vecteur-vecteur0)/2)
                vecteur0=vecteur
            #calcul vitesse moyenne pour 12 frame (=1 seconde)
            tab.append(statistics.mean(tab))
            y.append(tab[-1])
            lab.append(slbl[0])
            x.append(s)
            tab=[]

            s+=1

        # calcul vitesse moyenne / personne pour la video entière
        for i in x :
            pred.append(statistics.mean(y))
        # calcul moyenne glissante sur 6 sec
        taille = 6
        mat=np.array([1/taille]*taille)
        res=np.convolve(y, mat, 'valid')
        res=np.append([res[0]]*int(taille/2), res)
        res=np.append(res, [res[-1]]*int((taille/2)-1))

        # si moyenne glissante >= moyenne video ==> 1
        for p, ref in zip(res, pred):
            if p>=ref :
                prediction.append(1)
            else :
                prediction.append(0)
        prediction = np.array(prediction)
        prediction = ndimage.binary_erosion(prediction).astype(prediction.dtype)

        dico_prediction[key]=prediction

        conf, rate = check(list(prediction), lab, key, name)
        predictions.append(np.array(prediction)*int(key))
        rates.append(rate)

        graphVitesses(x, y, res, pred, key, name)

    with open('./'+name+'/speaking_PRED.pickle', 'wb') as handle:
        pickle.dump(dico_prediction, handle, protocol=pickle.HIGHEST_PROTOCOL)
    graphResultats(rates, lms, slb, colorbar, x, predictions, name)



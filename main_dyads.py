# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 14:42:17 2019

@author: piotm
"""
import pickle
import speaking
import dyades

def chargementPickles(name):
    lms = pickle.load(open('./'+name+'/landmarks.pickle', 'rb'))
    slb = pickle.load(open('./'+name+'/speaking_labels.pickle', 'rb'))
    vec = pickle.load(open('./'+name+'/vector3D.pickle', 'rb'))
    dlb = pickle.load(open('./'+name+'/dyade_labels.pickle', 'rb'))
    ctr = pickle.load(open('./'+name+'/centres.pickle', 'rb'))
    return lms, slb, vec, dlb, ctr

name = 'Video 3v2FR'
print("=== LOADING DATA ===")
lms, slb, vec, dlb, ctr = chargementPickles(name)
print("=== PROCESSING SPEAKING ===")
speaking.processing(lms, slb, name)
print("=== PROCESSING DYADE ===")
spk = pickle.load(open('./'+name+'/speaking_PRED.pickle', 'rb'))
dyades.processing(name, vec, dlb, ctr, spk)

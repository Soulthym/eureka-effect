# -*- coding: utf-8 -*-
import os, re
from face_detector import FaceDetector, FaceIdentifier
from itertools import count
import csv
from video import Video
from copy import deepcopy
import cv2
from sys import platform, exit
from track import full_tracker
import time
from pprint import pprint, pformat
from rotate import orient_normalize_3d_landmarks
from utils.repr import get_time_repr
import numpy as np

class Extractor():
    def __init__(self, path_to_video=0,show=True, device='cpu',
                 autofill_names=[], max_frame=None, start_frame=0):
        print("Launching initialisation")
        self.path_to_video = path_to_video
        self.show = show
        self.device = device
        self.names = autofill_names
        self.max_frame=max_frame
        self.start_frame=start_frame
        print(f"Parameters : \n{self.__dict__}")
        self.FD = FaceDetector(device=device)
        self.FI = FaceIdentifier()

    def askUser (self, profile):
        if self.show:
            cv2.namedWindow("Face detected", cv2.WINDOW_NORMAL)
            for i in range (3):
                cv2.imshow("Face detected", profile)
                cv2.waitKey(1)
            cv2.imshow("Face detected", profile)
            cv2.waitKey(1)
        nom = input ("Entrez le nom de la personne présentée sur l'image : ")
        if nom.lower().strip() in ('0','none','null','',0):
            nom = '0'
        if self.show:
            cv2.destroyAllWindows()
        return nom

    def extract(self):
        """
        Extracts every detected face's full profile for every frame of the video
        """
        print("Starting Extraction")
        os.makedirs(".tmp", exist_ok=True)
        tmpvideo = Video(path=self.path_to_video, outpath=".tmp",
                         start_frame=self.start_frame)
        with open(".tmp/tmp","w+") as tmpf:
            tmpf.write("frame;rectangle;embedding;landmarks\n")
            for frame_nb in count(1):
                if tmpvideo.read() is None:
                    break
                if self.max_frame and frame_nb > self.max_frame:
                    break
                faces = self.FD.detect(tmpvideo.frame)
                print(f"Pre-process : Frame {frame_nb+self.start_frame} | {len(faces)} faces detected")
                for face in faces:
                    tmpf.write(
                            ";".join([repr(frame_nb),
                                ",".join([repr(face["faceRectangle"][i])
                                    for i in ['left', 'top', 'width', 'height']]) ,
                                re.sub('\s','',re.sub(r'array\(\[([0-9e \n-.]*)]\)','\g<1>', repr(face["faceEmbeddings"]))) ,
                                re.sub("\s|[\(\)]","",",".join([repr((
                                    face["faceLandmarks"][repr(lm)]["x"],
                                    face["faceLandmarks"][repr(lm)]["y"],
                                    face["faceLandmarks"][repr(lm)]["z"]))
                                        for lm in range(68)]))]) + "\n")
                                #repr(face["emotions"])])+ "\n")
                if len(self.names) <= 0:
                    for face in faces:
                        b = [face["faceRectangle"][i]
                                for i in ['left', 'top', 'width', 'height']]
                        self.names.append(
                            self.askUser(tmpvideo.frame[b[1]:b[1]+b[3],
                                                        b[0]:b[0]+b[2]]))
                    print(self.names)
                if self.show:
                    cv2.imshow('YOLO Face detection', tmpvideo.frame)
                if cv2.waitKey(1) & 0xff == 27:
                    break
        tmpvideo.stream.release()
        if self.show:
            cv2.destroyAllWindows()
        return self

    def track(self):
        embedding_tolerance=0.67
        self.frames = full_tracker(max_frame=self.max_frame,
                                   autofill_names=self.names,
                                   embedding_tolerance=embedding_tolerance)
        print("Result")
        cam = cv2.VideoCapture(self.path_to_video)
        self.framerate = cam.get(cv2.CAP_PROP_FPS)
        if self.show:
            cv2.namedWindow("Face detected", cv2.WINDOW_NORMAL)
            cv2.setWindowProperty("Face detected", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        for fnb, frame in sorted(self.frames.items()):
            ret_val, img = cam.read()
            for face in frame:
                conf, name = face["conf"], face["name"]
                if name != "unknown":
                    dist=(1-conf)
                    for lm in face["lms"]:
                        cv2.circle(img, (int(lm[0]), int(lm[1])), 2, (255,255,255), 2, 1)
                    x,y,w,h = int(face["box"][0]), int(face["box"][1]), int(face["box"][2]), int(face["box"][3])
                    cv2.circle(img, (int(x+w/2),int(y+h/2)), 2, (0,0,255), 2, 1)
                    if conf > embedding_tolerance:
                        cv2.rectangle(img, (x,y), (x+w, y+h), (0,int(conf*255),int(dist*255)), 4)
                        cv2.putText(img, f"{name}: {conf:.2f}", (x,y-3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2)
                    else:
                        cv2.rectangle(img, (x,y), (x+w, y+h), (0,0,255), 4)
                        cv2.putText(img, f"{name}: {conf:.2f}", (x,y-3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2)
            if self.show:
                cv2.imshow('Face detected', img)
                if cv2.waitKey(1) == 27:
                    break  # esc to quit
        if self.show:
            cv2.destroyAllWindows()
        cam.release()
        return self

    def rotate_landmarks(self):
        for fnb, frame in self.frames.items():
            for face in frame:
                #print(type(face), face.keys())
                face["orig-lms"] = face["lms"]
                if np.linalg.norm(face["lms"]) > 0:
                    face["ang"], face["lms"] = orient_normalize_3d_landmarks(face["lms"])
                else:
                    face["ang"] = (0.,0.,0.)
                    print(f"{fnb}: {face['name']} was not found")
        return self

    def keep_frames(self, fps=12):
        kept_frames = np.hstack([np.round(np.linspace(i, self.framerate+i, fps+1)).astype(int)[:-1]
            for i in range(1, max(self.frames.keys())+2, int(self.framerate))])
        self.frames= {k: self.frames[k]
                      for k in kept_frames
                      if k in self.frames.keys()}
        return self

    def save(self, out_file=None):
        if out_file is None:
            out_file = str(self.path_to_video).split("/")[-1].split(".")
            if len(out_file) > 1:
                out_file = ".".join(out_file[:-1])
            else:
                out_file = ".".join(out_file)
            out_file = f"out/data-{out_file}.csv"
        with open(out_file ,"w+") as outf:
            cleanarray = lambda s: re.sub('\s','',
                                          re.sub(r'array\(([0-9e \n-.,]*)\)','\g<1>',
                                                 re.sub('[\[\]]','', repr(s.flatten()))))
            outf.write("frame;time;name;box;centre;vector3D;angle3D;landmarks;is_speaking;emotions\n")
            for fnb, frame in sorted(self.frames.items()):
                for f in sorted(frame, key=lambda k: k["name"]):
                    if f["name"] != "unknown":
                        x,y,w,h = (int(f["box"][i]) for i in range(4))
                        outf.write(";".join([repr(fnb),
                                             get_time_repr(frame_number=fnb,
                                                           framerate=self.framerate),
                                             repr(f["name"]).replace('\'',''),
                                             cleanarray(f["box"].astype(int)),
                                             cleanarray(f["cen"].astype(int)),
                                             cleanarray(
                                                 np.vstack(
                                                     (np.mean(f["orig-lms"],
                                                              axis=0),
                                                      f["orig-lms"][30]))),
                                             cleanarray(np.array(f["ang"])),
                                             cleanarray(f["lms"]),
                                             repr(None), #emotion_classifier.get_emotion(img[y:y+h,x:x+w])),
                                             repr(None),
                                            ])+"\n")
        if self.show:
            cv2.destroyAllWindows()

if __name__ == "__main__":
    extractor = Extractor("videos/00042.MTS", show=False, device='cpu',
                          autofill_names=["2","1","0","3","0","4","0"],
                          max_frame=None).track().rotate_landmarks().keep_frames().save("output")
                          #max_frame=None).extract().track().rotate_landmarks().save()
                          #max_frame=50).extract().track().save()

#! /bin/python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 17:17:53 2019

@author: piotm, soulthym
"""
from argparser import Parser
import cv2
from itertools import count
from extractor import Extractor
import interaction_detector

args = Parser()
print(args)
extractor = Extractor(path_to_video=args.input_file[0],
                      show=not args.cli,
                      device=args.device,
                      autofill_names=args.names,
                      max_frame=args.max_frame,
                      start_frame=args.start_frame,
                      ).extract()

if not args.pre_process_only:
    extractor.track().rotate_landmarks().keep_frames().save()

    name = args.input_file[0].split("/")[-1].split(".")[0]
    interaction_detector.process(name=name)

print("Finished extraction")
cv2.destroyAllWindows()

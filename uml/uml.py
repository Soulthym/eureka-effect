import subprocess
import os
pyreverse = subprocess.Popen("pyreverse -o png -p new_main sujet_definition.py video_definition.py deepsight_face_detector.py new_main.py".split())
pyreverse.wait()

for item in os.listdir("."):
    if item.endswith(".pyc"):
        os.remove(item)
    if item.endswith(".png"):
        os.rename(item, "uml/"+item)

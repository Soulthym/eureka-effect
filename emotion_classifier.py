from time import sleep
from keras.models import load_model
from keras.preprocessing.image import img_to_array
import numpy as np
import cv2
from pprint import pprint
import sys
from video import Video

emotion_model_path="./emotion_recognition/models/_mini_XCEPTION.102-0.66.hdf5"
emotion_classifier = load_model(emotion_model_path, compile=False)
emotion_labels = ["anger",
                  "disgust",
                  "fear",
                  "joy",
                  "sadness",
                  "surprise",
                  "neutral"]
input_dim = emotion_classifier.input_shape[1:3]

def get_emotion(img):
    if img is not None and len(img) != 0:
        input_data = np.expand_dims(
            img_to_array(
                cv2.resize(
                    cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
                    input_dim
                ).astype("float")/255.0), axis=0)
        return {k:v for k,v in zip(emotion_labels, emotion_classifier.predict(input_data)[0])}
    return {}

if __name__ == "__main__":
    try:
        path_to_video = int(sys.argv[1])
    except:
        path_to_video = sys.argv[1]
    v = Video(path_to_video)
    root = ".".join(path_to_video.split("/")[-1].split(".")[:-1])
    in_file = f"out/data-{root}.csv"
    img = v.read()
    with open(f"{in_file}", "r") as inf:
        with open(f"{in_file}.out", "w+") as outf:
            header = inf.readline()
            keys = header.strip().split(";")
            header = ";".join([
                k.replace("emotions",f"emotions({','.join([e for e in emotion_labels])})")
                for k in keys])
            outf.write(header.strip()+"\n")
            print(header)
            for l in inf.readlines():
                face = {k:v for k,v in zip(keys, l.strip().split(";"))}
                print(face["frame"], face["name"])
                x,y,w,h = (int(face["box"].split(",")[i]) for i in range(4))
                while int(face["frame"]) > v.num_frame:
                    img = v.read()
                #print(int(face["frame"]) - v.num_frame, face["frame"], v.num_frame)
                if (x,y,w,h) != (0,0,0,0):
                    faceimg = img[y:y+h,x:x+w]
                    #print([a-b for a,b in zip(faceimg.shape[:2], (h,w))])
                    emotions = get_emotion(faceimg)
                    face["emotions"] = ",".join([str(emotions[k]) for k in emotion_labels])
                    #print(face["emotions"])
                    outf.write(";".join(str(face[k]) for k in keys)+"\n")
    cv2.destroyAllWindows()

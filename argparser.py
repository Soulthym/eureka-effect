#! /bin/python3
import argparse

def Parser():
    parser = argparse.ArgumentParser(description='Eureka CLI')
    parser.add_argument("-p","--pre-process-only",
            action='store_true',
            help="Pre-processing only run, extracts all detected faces without performing identification, on a frame by frame basis. Stored as .tmp/tmp")
    parser.add_argument("-c","--cli",
            action='store_true',
            help="CLI only run, no image is shown to the user, only stored in a video file unless specified otherwise")
#    parser.add_argument("-a","--auto-fill",
#            action='store_true',
#            help="if set, names are going to be automatically assigned by the order they where detected in")
    parser.add_argument("-n","--names",
            type=str, default=[], nargs="+",
            help="specify the names to be auto-inputed")
    parser.add_argument("-i","--input-file",
            type=str, default=[0], nargs="+",
            help="Specify one or more video sources")
    parser.add_argument("-s","--start-frame",
            type=int, default=0,
            help="Specify the frame the extractor needs to start at, default value: None")
    parser.add_argument("-m","--max-frame",
            type=int, default=None,
            help="Specify the maximum number of frames the extractor needs to run on, default value: None")
    parser.add_argument("-d","--device",
            type=str, default="cpu",
            choices=["cpu","cuda"],
            help="Specify which device to run on ")
    args = parser.parse_args()
    return args
if __name__ == "__main__":
    args = Parser()
    print(args)
